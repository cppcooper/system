function br(cr)
  (8*cr*(1000^2))/(1000^2)
end

function cr(br)
  (br*(1000^2))/(8*(1000^2))
end

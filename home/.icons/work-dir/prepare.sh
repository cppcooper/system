#!/usr/bin/bash
trap 'exit 1' ERR

cd $(dirname $0)
source ./functions.bash

trap 'echo $BASH_COMMAND' DEBUG
rm -rf final/
rm -rf pre-final/
rm -rf symlinks/
copy_symlinks papirus/ symlinks/
copy_nonsymlinks papirus/ pre-final/
delete_matches pre-final/ files-base-trash
delete_matches symlinks/ files-base-trash
#base setup is done

copy_matches elementary/ pre-final/ files-elementary
copy_matches adwaita/ pre-final/ files-adwaita
copy_all special-icons/ pre-final/
# elementary [x]
# special-icons [x] --icons that I have hand selected/copied/named/placed/etc.

# there are a bunch of symlinks pointing to blue variants of folder-download(s)
find symlinks -name "*folder-*-download*" -delete

# symlinks and pre-final have conflicts
# we want the symlinks to be replaced
copy_all symlinks/ final/
copy_all pre-final/ final/
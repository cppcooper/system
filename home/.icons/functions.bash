function delete_matches(){
    if [ -z "$1" ] || [ -z "$2" ]; then
        echo "usage: delete_matches search_dir files"
        return 1
    fi
    for file in $(cat $2); do
        find $1 -iwholename "*$file" -delete
    done
    find $1 -type d -empty -delete
}

function copy_all(){
    if [ -z "$1" ] || [ -z "$2" ]; then
        echo "usage: copy_all source dest files"
        return 1
    fi
    for file in $(find $1 -not -type d); do
        file=${file/$1/}
        dir=$2$(dirname $file)
        mkdir -p $dir
        cp $1$file $2$file
    done
}

function copy_files(){
    if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
        echo "usage: copy_files source dest files"
        return 1
    fi
    for file in $(cat $3); do
        dir=$2$(dirname $file)
        mkdir -p $dir
        cp $1$file $2$file
    done
}

function copy_matches(){
    if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
        echo "usage: copy_matches source dest files"
        return 1
    fi
    for line in $(cat $3); do
        for src in $(find $1 -iwholename "*$line"); do
            dest=${src/$1/$2}
            dir=$(dirname $dest)
            mkdir -p $dir
            cp $src $dest
        done
    done
}

function copy_symlinks(){
    if [ -z "$1" ] || [ -z "$2" ]; then
        echo "usage: copy_symlinks source dest"
    return 1
    fi
    for file in $(find $1 -not -type d,f); do
        newfile=${file/$1/$2}
        dir=$(dirname $newfile)
        mkdir -p $dir
        cp $file $newfile
    done
}

function copy_nonsymlinks(){
    if [ -z "$1" ] || [ -z "$2" ]; then
        echo "usage: copy_symlinks source dest"
        return 1
    fi
    for file in $(find $1 -type f); do
        newfile=${file/$1/$2}
        dir=$(dirname $newfile)
        mkdir -p $dir
        cp $file $newfile
    done
}











function matches_all(){
    if [[ -z "$1" ]]; then
        return 1;
    fi
    declare -A map
    for line in $(cat $1); do
        map[$line]=1
    done
    for file in $(find . -not -type d); do
        f=$(basename $file);
        if [[ ${map[$f]} ]]; then
            continue;
        else
            echo "match missed"
            return 1
        fi;
    done
    echo "matched all"
}


function compare_conflicts(){
    if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
        echo "usage: compare_conflicts base top conflicts"
        return 1
    fi
    for line in $(cat $3); do
        dir="comparison/$(dirname $line)"
        mkdir -p $dir
        cp "$1/$line" "comparison/${line}-base"
        cp "$2/$line" "comparison/${line}"
    done
}
